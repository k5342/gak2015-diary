<?php
require 'include.php';

if(is_login()){
	header('location: ./');
	exit();
}

$msg = 'ログインしてください';

try{
	if (isset($_POST['id']) && isset($_POST['password'])){
		if (is_string($_POST['id']) && is_string($_POST['password'])){
			$u = new User(null, $_POST['id']);
			if ($u -> id !== null){
				$status = password_verify($_POST['password'], $u -> password);
				$db = new SQLite('diary.sqlite');
				$db -> insert('login_history', array(
					'user_id' => $u -> id,
					'timestamp' => time(),
					'ip' => $_SERVER['REMOTE_ADDR'],
					'success' => $status,
				));
				if ($status){
					
					if (password_needs_rehash($u -> password, PASSWORD_DEFAULT)){
						$u -> password = password_hash($_POST['password'], PASSWORD_DEFAULT);
						$u -> save();
					}
					
					$_SESSION['is_login'] = true;
					$_SESSION['user'] = $u;
					
					session_regenerate_id();
					
					if (isset($_GET['continue']) && !empty($_GET['continue'])){
						$path = $_GET['continue'];
					}else{
						$path = './';
					}
					
					header('location: ' . $path);
					exit();
				}
			}
		}
		throw new Exception('ログイン失敗');
	}
}catch(Exception $e){
	$msg = $e -> getMessage();
}

$page = new Page("ログイン");
$page -> loginRequired(false);
$page -> putHeader();

$msg = "<p>$msg</p>";
?>
<h2>ログイン</h2>
<form method="post">
  <fieldset>
    <?=@$msg?>
    <input type="text" name="id" placeholder="ID" />
    <br />
    <input type="password" name="password" placeholder="パスワード" />
    <br />
    <input type="submit" name="submit" value="ログイン" />
  </fieldset>
</form>
