<?php
require 'include.php';

$success = false;

try{
	if(isset($_POST['submit'])){
		if(empty($_POST['password'])
			|| empty($_POST['password_new'])
			|| empty($_POST['password_new_confirm'])){
			throw new Exception('未入力項目有り');
		}
		if(!isset($_POST['token']) || !verify_token($_POST['token'])){
			throw new Exception('不正なリクエスト');
		}
		if(password_verify($_POST['password'], $_SESSION['user'] -> password)){
			if($_POST['password_new'] === $_POST['password_new_confirm']){
				$_SESSION['user'] -> password =
					password_hash($_POST['password_new'], PASSWORD_DEFAULT);
				$_SESSION['user'] -> save();
				$success = true;
			}else{
				throw new Exception('パスワードが一致しません');
			}
		}else{
			throw new Exception('無効なパスワード');
		}
	}
}catch(Exception $e){
	$msg = "<p>".$e -> getMessage()."</p>";
}

regenerate_token();
$token = getToken();

$page = new Page('パスワード変更');
$page -> putHeader();
?>
<a href="./">トップ画面に戻る</a>
<?php
if($success){
?>

<p>パスワード変更しました。</p>
<?php
}else{
?>

<section>
<h2>パスワード変更</h2>
<?=@$msg?>
<form method="post">
<fieldset>
<label>現在のパスワード</label>
<input type="password" name="password" value="" autofocus required/>
<br />
<label>新しいパスワード</label>
<input type="password" name="password_new" value="" required/>
<br />
<label>新しいパスワード(確認)</label>
<input type="password" name="password_new_confirm" value="" required/>
<br />
<input type="hidden" name="token" value="<?=$token?>" />
<input type="submit" name="submit" value="送信" />
</div>
</fieldset>
</form>
</section>

<?php
}
$page -> putFooter();
