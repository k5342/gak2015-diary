<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="徳山高専 第41回高専祭のブログ。高専祭準備期間中の新着情報や企画についての案内を少しずつ発表していきます！">
	<title><?=h($title)?> | 第41回高専祭</title>
	<link rel="stylesheet" href="../styles.css">
</head>
<body>
	<header>
		<div class="container wrap">
			<a class="title" href="../">
				<h1 class="background background-logo dynamic">第41回高専祭 <span class="theme">4link</span></h1>
			</a>
			<nav>
				<ul>
					<li><a href="../about.html"><span class="about">高専祭について</span></a></li>
					<li><a href="../main.html"><span class="main">メイン企画</span></a></li>
					<li><a href="../map.html"><span class="map">会場案内</span></a></li>
					<li><a href="../event.html"><span class="event">イベント</span></a></li>
					<li><a href="../access.html"><span class="access">アクセス</span></a></li>
				</ul>
			</nav>
		</div>
	</header>
	<div class="pane">
		<div class="container">
			<div class="container-split">
				<div class="container-left">
					<article>
						<div class="article-header">
<?php
if ($countdown_panel != 'xx'){
?>

							<div class="background background-blue panel counter counter-small center">
								<div class="logo logo-150px"></div>
								<?=h($countdown_prefix)?>
<?php
foreach ($countdown_panel_array as $x){
?>

								<span class="num-<?=h($x)?>"></span>
<?php
}
?>

								<?=h($countdown_suffix)?>

							</div>
<?php
}
?>

							<h3>
								<?=h($title)?>

								<div class="postinfo"><?=$display_date?></div>
							</h3>
						</div>
						<section>
							<?=n2br(expandTag(h($body), "../../diary/"))?>

						</section>
					</article>
				</div>
				<div class="container-right">
					<div class="panel panel-tight">
						<h3>徳山高専学生会 新着記事</h3>
						<dl>
<?php
if ($next !== null){
?>

							<dt>次の記事</dt>
							<dd><a href="<?=getPath($res[$next], $r)?><?=getFilename($res[$next])?>.html"><?=h($res[$next]['title'])?></a></dd>
<?php
}
if ($prev !== null){
?>

							<dt>前の記事</dt>
							<dd><a href="<?=getPath($res[$prev], $r)?><?=getFilename($res[$prev])?>.html"><?=h($res[$prev]['title'])?></a></dd>
<?php
}
?>

						</dl>
					</div>
					<div class="panel panel-tight">
						<h3>高専祭についての記事</h3>
						<?=$fes_article_list?>

					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="jumbotron">
			<div class="container wrap">
				<div class="container container-full">
					<div class="center">
						<a href="http://tctgak.web.fc2.com/">
							<img class="center" alt="徳山高専学生会" src="../images/gakuseikai.png" />
						</a>
						<hr />
						<ul class="inline">
							<li><a href="http://tctgak.web.fc2.com/index.html" target="_blank">徳山高専学生会トップページ</a></li>
							<li><a href="http://tctgak.web.fc2.com/meyasubako.html" target="_blank">目安箱</a></li>
							<li><a href="http://tctgak.web.fc2.com/schedule.html" target="_blank">年間スケジュール</a></li>
							<li><a href="https://twitter.com/nittc_gak" target="_blank" rel="nofollow">徳山高専学生会 Twitter</a></li>
						</ul>
					</div>
				</div>
			</div>
	</footer>
</body>
</html>
