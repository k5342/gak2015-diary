<?php
require 'include.php';

if(!is_admin()
	|| !isset($_GET['id'])
	|| !isset($_GET['token'])
	|| $_GET['token'] !== $_SESSION['token']
	|| !USE_APPROVAL
){
	header('location: ./');
	exit();
}

if(isset($_GET['id'])){
	$article = new Diary($_GET['id']);
	if ($article -> getStatus() < DIARY_ACCEPTED){
		$article -> setStatus(DIARY_ACCEPTED);
		$res = $article -> save();
		
		if ($res){
			$msg = '<p class="sysmsg info">記事「' . h($article -> title) . '」を承認しました。</p>';
		}else{
			$msg = '<p class="sysmsg error">書き込みエラー。</p>';
		}
	}else{
		$msg = '<p class="sysmsg error">既に承認されているようです。</p>';
	}
}
$page = new Page("記事承認");
$page -> putHeader();
?>
<?=@$msg?>

<a href="./">トップ画面に戻る</a>

<?php
$page -> putFooter();
