<?php
require 'include.php';

$db = new SQLite('diary.sqlite');
$res = $db -> fetchAll('diary', true);

regenerate_token();
$token = getToken();

$page = new Page();
$page -> putHeader();
?>
  <header>
    <h1>学生会HP記事管理システム</h1>
  </header>
  <section>
    <h2>メニュー</h2>
    <p>
      現在<?=h($_SESSION['user'] -> name)?>でログイン中です
      (
      管理権限: <?=(is_admin()) ? '有' : '無'?>,
      準管理権限: <?=(is_modelator()) ? '有' : '無'?>
      )
    </p>
    <ul>
      <li><a href="./new.php">投稿</a>
      <li><a href="./user-manage.php">ユーザ管理</a>
      <li><a href="./password.php">ログインパスワード変更</a>
      <li><a href="./logout.php">ログアウト</a>
    </ul>
  </section>
  <section>
    <h2>TODO:</h2>
    <ul>
      <li>更新時のメール通知機能</li>
      <li>記事の編集履歴管理機能</li>
      <li>記事の確認時のコメント機能(否認理由とか書けるように)</li>
      <li>
          タグのみ編集した場合の承認スキップ機能<br />
          →例えば、改行追加だけで承認を得るのは手間
      </li>
    </ul>
  </section>
  <section>
    <h2>使い方</h2>
    <ul>
      <li>学生会HPに記事を投稿できます</li>
      <li>投稿は、画像(JPG, PNG, GIF)を複数枚添付することができます</li>
      <li>画像のアップロードと添付は記事の投稿画面から行えます</li>
      <li>記事は数分おきに勝手に本番環境へ同期されます</li>
<?php
if(USE_APPROVAL){
?>

      <li>投稿した記事は担当教員が承認してから公開されます</li>
      <li>編集しなおした記事も担当教員が承認してから公開されます</li>
      <li>(新規投稿→承認済→生成済→公開処理中→公開済)</li>
<?php
}else{
?>

      <li>(新規投稿→生成待ち→生成済→公開処理中→公開済)</li>

<?php
}
?>

    </ul>
  </section>
  <section>
    <h2>記事一覧</h2>
	<table class="lined">
		<tbody>
    	<tr>
			<th class="nowrap">#</th>
			<th colspan="2">タイトル</th>
			<th>投稿日</th>
			<th>更新日時</th>
			<th>状態</th>
			<th></th>
		</tr>
    <?php
    foreach($res as $r){
		$id    = $r['id'];
		$password = $r['password'];
		$title = $r['title'];
		$body  = $r['body'];
		$status = $r['status'];
		$created_at = $r['created_at'];
		
		if ($status >= DIARY_GENERATED){
			$highlight_tr = ' class="green"';
		}else{
			$highlight_tr = '';
		}
		
		$display_updated_at = date("Y/m/d H:i:s", $r['updated_at']);
		if (time() - $r['updated_at'] <= 60*60){
			$display_updated_at .= '<br />' . elapsedTime($r['updated_at'], time());
		}else{
		}
		
		if($status < DIARY_ACCEPTED){
			$display_status = bold(h($diaryStatus[$status]));
		}else{
			$display_status = h($diaryStatus[$status]);
		}
    ?>
	
		<tr<?=$highlight_tr?>>
			<td class="nowrap"><?=h($id)?></td>
			<td><a href="./view.php?id=<?=h($id)?>"><?=h($title)?></a></td>
			<td><?=(empty($password)) ? '' : '(鍵)' ?></td>
			<td><?=h(date("Y/m/d", $created_at))?></td>
			<td><?=$display_updated_at?></td>
			<td><?=$display_status?></td>
<?php
if(USE_APPROVAL && is_admin()){
	if($status < DIARY_ACCEPTED){
?>

			<td><a href="./approval.php?id=<?=$id?>&token=<?=$token?>">承認</a></td>
<?php
	}else{
?>

			<td><span class="disabled">承認</span></td>

<?php
	}
}
if(USE_APPROVAL && is_admin()){
	if($status >= DIARY_ACCEPTED){
?>

			<td><a href="./reject.php?id=<?=$id?>&token=<?=$token?>">否認</a></td>
<?php
	}else{
?>

			<td><span class="disabled">否認</span></td>

<?php
	}
}
?>

			<td><a href="./new.php?id=<?=$id?>">編集</a></td>
			<td><a href="#" onclick="if(deleteConfirm(this)){location.href='./delete.php?id=<?=$id?>&token=<?=$token?>'}; return false;">削除</a></td>

		</tr>
    <?php
    }
    ?>

		</tbody>
	</table>
</section>
<script>
function deleteConfirm(e){
	var title = e.parentNode.parentNode.childNodes[3].innerText
	var t = prompt("記事「" + title + "」を削除してもよろしいですか？\n本当に削除する場合はタイトルを入力します。");
	return t == title;
}
</script>
<?php
$page -> putFooter();
