<?php
require 'include.php';

if(!isset($_POST['body']) || is_array($_POST['body'])){
	header('Location: ./');
	exit();
}

echo n2br(expandTag(h($_POST['body'])));
exit();
