<?php
session_start();

$_SESSION['is_login'] = false;

unset($_SESSION['is_login']);
unset($_SESSION['user']);

if (isset($_COOKIE['PHPSESSID'])){
	setcookie('PHPSESSID', '', time()-2592000);
}

header('location: ./login.php');
session_destroy();
exit();
