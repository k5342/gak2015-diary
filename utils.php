<?php
class SQLite{
	private $db;
	function __construct($name = ""){
		$this -> db = new PDO("sqlite:$name");
		$this -> db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		$this -> db -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}
	function escape($str){
		if($str === null){
			return 'null';
		}else{
			return $this -> db -> quote($str);
		}
	}
	function insert($tbl_name, $data){
		$values = array_map(array($this, 'escape'), $data);
		return $this -> db -> exec("INSERT INTO $tbl_name (". implode(",", array_keys($data)) .") VALUES (". implode(",", $values) .")");
	}
	
	function delete($tbl_name, $expr){
		return $this -> db -> exec("DELETE FROM $tbl_name WHERE $expr");
	}
	
	function update($tbl_name, $data, $expr = '1'){
		$values  = array_map(array($this, 'escape'), $data);
		$updates = array();
		foreach($values as $k => $v){
			$updates[] = "$k = $v";
		}
		return $this -> db -> exec("UPDATE $tbl_name SET ". implode(", ", $updates) ." WHERE $expr");
	}
	function fetchWithExpr($tbl_name, $expr, $reverse = false){
		$sort = ($reverse) ? 'DESC' : 'ASC';
		$stmt = $this -> db -> prepare("SELECT * FROM $tbl_name WHERE $expr ORDER BY id $sort;");
		$stmt -> execute();
		return $stmt -> fetchAll(PDO::FETCH_ASSOC);
	}
	function fetchAll($tbl_name, $reverse = false){
		$sort = ($reverse) ? 'DESC' : 'ASC';
		$stmt = $this -> db -> prepare("SELECT * FROM $tbl_name ORDER BY id $sort;");
		$stmt -> execute();
		return $stmt -> fetchAll(PDO::FETCH_ASSOC);
	}
	function &getInstance(){
		return $this -> db;
	}
}

function h($str){
	return htmlspecialchars($str, ENT_QUOTES);
}
function n2br($str){
	$buf = '';
	
	$lines = preg_split("/\r?\n/m", $str);
	
	foreach($lines as $l){
		if (preg_match('/.+(?<=>(?<=$))$/', $l) === 0
			|| preg_match('/span>|strong>/', $l) === 1
			|| empty($l)
		){
			$buf .= "${l}<br />";
		}else{
			$buf .= "${l}";
		}
	}
	return rtrim($buf, '<br />');
}
function chomp($str) {
	return rtrim(rtrim($str, "\n"), "\r");
}
function stripTag($body){
	$body = preg_replace("/\r/", "", $body);
	$body = preg_replace("/^\n/", "", $body);
	$body =  preg_replace("/\[\/?(h[1-6]|b|size|color|pre|hr|img) ?.*?\]\n?/", "", $body);
	return $body;
}
function expandTag($body, $root = "./"){
	$body =  preg_replace_callback('/\[(\w+?)( (.+?)|)\]/',
		function($matches) use ($root){
			if(!isset($matches) || !$matches){
				return;
			}
			switch((string)$matches[1]){
				default:
					return $matches[0];
			}
		},
		$body);
	
	$single_tag = '\[(\w+?)( ([^\]]+?)|)\]';
	$twist_tag  = '\[(\w+?)( ([^\]]+?)|)\](.*?)\[\/\2\]';
	$pattern    = "/(${twist_tag}|${single_tag})/s";
	
	$expand = function ($body) use (&$expand, $pattern, $root) {
		return preg_replace_callback($pattern,
			function($matches) use (&$expand, $pattern, $root){
				if(!isset($matches) || !$matches){
					return;
				}
				
				$orig  = $matches[0];
				$close = $matches[5];
				if (count($matches) <= 6){
					# twist tag
					$tag   = $matches[2];
					$attr  = $matches[4];
					if (preg_match($pattern, $close)){
						$inner = $expand($close);
					}else{
						$inner = $close;
					}
				}else{
					# single tag
					$tag   = $matches[6];
					$attr  = @$matches[8];
					$inner = $orig;
				}
				
				switch(strtolower($tag)){
					case 'h1':
						return "<h1>${inner}</h1>";
					case 'h2':
						return "<h2>${inner}</h2>";
					case 'h3':
						return "<h3>${inner}</h3>";
					case 'h4':
						return "<h4>${inner}</h4>";
					case 'h5':
						return "<h5>${inner}</h5>";
					case 'h6':
						return "<h6>${inner}</h6>";
					case 'b':
						return "<strong>${inner}</strong>";
					case 'size':
						return '<span style="font-size: ' . h($attr) . ';">' . $inner . '</span>';
					case 'color':
						return '<span style="color: ' . h($attr) . ';">' . $inner . '</span>';
					case 'pre':
						return "<pre>${close}</pre>";
					case 'hr':
						return '<hr />';
					case 'img':
						return '<img src="' . $root . 'images/' . h($attr) . '" />';
					default:
						return $inner;
				}
			},
			$body);
	};
	
	return preg_replace('/<(\/ ?|)pre>[\r\n]*/i', '', $expand($body));
}
function msg($str){
	if(empty($str)){
		return '';
	}
	return '<p class="msgbox">' . h($str) . '</p>';
}

function bold($str){
	return "<strong>$str</strong>";
}

function elapsedTime($time_int_begin, $time_int_end){
	$time_int = $time_int_end - $time_int_begin;
	
	$sec  = (int) $time_int % (60);
	$min  = (int)($time_int % (60*60)    / (60));
	$hour = (int)($time_int % (60*60*24) / (60*60));
	$day  = (int)($time_int              / (60*60*24));
	$str  = '';
	
	if ($day != 0){
		$str .= "${day}日";
	}
	if ($hour != 0){
		$str .= "${hour}時間";
	}
	if ($min != 0){
		$str .= "${min}分";
	}
	if ($sec != 0){
		$str .= "${sec}秒";
	}
		
	if ($time_int < 0){
		$str .= '後';
	}else{
		$str .= '前';
	}
	
	return $str;
}

function is_login(){
	return isset($_SESSION) && isset($_SESSION['is_login']) && $_SESSION['is_login'] && $_SESSION['user'];
}

function is_admin(){
	return (bool)$_SESSION['user'] -> is_admin === true;
}

function is_modelator(){
	return (bool)$_SESSION['user'] -> is_modelator === true;
}

function regenerate_token(){
	$token = sha1(mt_rand());
	$_SESSION['token'] = $token;
}

function getToken(){
	return $_SESSION['token'];
}

function verify_token($token){
	return $_SESSION['token'] === $token;
}

function getCountdown($now_int = null){
	if (!$now_int){
		$now_int = time();
	}
	$countdown_remain = ceil((strtotime('2015/10/31') - $now_int) / (60 * 60 * 24));
	if ($countdown_remain > 0) {
		return '' . $countdown_remain;
	}
	if ($countdown_remain >= -1 and $countdown_remain <= 0) {
		return '' . (abs($countdown_remain) + 1) . 'd';
	}
	return 'xx';
}
