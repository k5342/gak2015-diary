<?php
require 'include.php';

try{
	if(isset($_POST['submit'])){
		if(!isset($_FILES['data']['error']) || !is_int($_FILES['data']['error'])){
			throw new RuntimeException('パラメータが不正');
		}
		switch($_FILES['data']['error']){
			case UPLOAD_ERR_OK:
				break;
			case UPLOAD_ERR_NO_FILE:
				throw new RuntimeException('ファイルを選択してください');
				break;
			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE:
				throw new RuntimeException('ファイルサイズの超過(サーバにアップロード可能なファイルサイズを超過しています)');
				break;
			default:
				throw new RuntimeException('予期せぬエラー');
		}
		if($_FILES['data']['size'] <= 0){
			throw new RuntimeException('ファイルを選択してください');
		}
		
		$data = $_FILES['data'];
		$img = new Imagick($data['tmp_name']);
		$signature = $img -> getImageSignature();
		
		if ($img -> getImageWidth() > 700){
			$img -> thumbnailImage(700, 0);
		}
		
		$img -> stripImage();
		
		$o = $img -> getImageOrientation();
		switch ($o) {
			case imagick::ORIENTATION_TOPRIGHT:		// 2	左右の鏡像
				$img -> flopImage();
				$img -> setimageorientation(imagick::ORIENTATION_TOPLEFT);
				break;
			case imagick::ORIENTATION_BOTTOMRIGHT:	// 3	180度回転
				$img -> rotateImage(new ImagickPixel(), 180);
				$img -> setimageorientation(imagick::ORIENTATION_TOPLEFT);
				break;
			case imagick::ORIENTATION_BOTTOMLEFT:	// 4	3+鏡像
				$img -> rotateImage(new ImagickPixel(), 270);
				$img -> flopImage();
				$img -> setimageorientation(imagick::ORIENTATION_TOPLEFT);
				break;
			case imagick::ORIENTATION_LEFTTOP:		// 5	6+鏡像
				$img -> rotateImage(new ImagickPixel(), 90);
				$img -> flopImage();
				$img -> setimageorientation(imagick::ORIENTATION_TOPLEFT);
				break;
			case imagick::ORIENTATION_RIGHTTOP:		// 6	右に90度回転
				$img -> rotateImage(new ImagickPixel(), 90);
				$img -> setimageorientation(imagick::ORIENTATION_TOPLEFT);
				break;
			case imagick::ORIENTATION_RIGHTBOTTOM:	// 7	8+鏡像
				$img -> rotateImage(new ImagickPixel(), 270);
				$img -> flopImage();
				$img -> setimageorientation(imagick::ORIENTATION_TOPLEFT);
				break;
			case imagick::ORIENTATION_LEFTBOTTOM:	// 8	右に270度回転
				$img -> rotateImage(new ImagickPixel(), 270);
				$img -> setimageorientation(imagick::ORIENTATION_TOPLEFT);
				break;
			case imagick::ORIENTATION_TOPLEFT:		// 1	そのまま
			case imagick::ORIENTATION_UNDEFINED:	// 0
			default:
				break;
		}
		
		if($img -> getImageLength() > 1024 * 1024 * 1024){
			throw new RuntimeException('ファイルサイズの超過(縮小後サイズが1MBを超過します)');
		}
		
		# generate file name
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		if(!$extension = @array(
			'image/gif' => 'gif',
			'image/jpg' => 'jpg',
			'image/jpeg' => 'jpg',
			'image/png' => 'png',
			)[$finfo -> file($data['tmp_name'])]
		){
			throw new RuntimeException('許可されていないファイルタイプ');
		}
		$fname = "${signature}.${extension}";
		$fpath = "images/${fname}";
		
		$img -> writeImage($fpath);
		chmod($fpath, 0644);
		//echo $fname;
		$msg = "正常にアップロードしました";
		$res = '<img src="' . $fpath . '" />';
	}
}catch(Exception $e){
	$msg = $e -> getMessage();
}

$page = new Page("画像を追加");
$page -> putHeader();
?>
<a href="#" onclick="window.close(); return false;">閉じる</a>

<?=msg(@$msg)?>
<?=@$res?>

<form method="post" enctype="multipart/form-data">
<fieldset>
<p>画像を選択します。</p>
<input type="file" name="data" accept="image/*" value="投稿"/>
<br />
<input type="submit" name="submit" value="投稿"/>
</fieldset>
</form>
<?php
$page -> putFooter();
