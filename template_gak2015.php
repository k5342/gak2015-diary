<?php
?>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?=h($title)?> | 徳山高専学生会</title>
        <meta name="description" content="">
        <meta name="author" content="徳山高専学生会">

        <meta name="viewport" content="width=1000px">

        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="apple-touch-icon" href="../apple-touch-icon.png">

        <link rel="stylesheet" href="../styles.css">
        <link rel="stylesheet" href="../normalize.css">
    </head>

    <body>
        <div id="container" class="diary">
            <header>
                <a href="../" title="徳山高専学生会TOP"><img src="../images/logo.png" alt="徳山高専学生会" /></a>
            </header>
            <section>
                <h2>記事</h2>
                <article>
                    <h3>
                        <?=h($title)?>

                        <div class="postinfo"><?=$display_date?></div>
                    </h3>
                    <section>
                        <?=n2br(expandTag(h($body)), "../diary/")?>

                    </section>
                </article>
            </section>
            <section class="neighbor-navigation">
<?php
if($next !== null){
?>

                <ul class="next">
                    <li>
                        次の記事<br />
                        <a href="<?=getPath($res[$next], $r)?><?=getFilename($res[$next])?>.html"><?=h($res[$next]['title'])?></a>
                    </li>
                </ul>

<?php
}
if($prev !== null){
?>

                <ul class="prev">
                    <li>
                        前の記事<br />
                        <a href="<?=getPath($res[$prev], $r)?><?=getFilename($res[$prev])?>.html"><?=h($res[$prev]['title'])?></a>
                    </li>
                </ul>
<?php
}
?>
                <div class="clear"></div>
            </section>
            <footer>
                <p>
                    &copy; 徳山高専学生会
                </p>
            </footer>
        </div>
    </body>
</html>
