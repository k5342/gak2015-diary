<?php
require 'include.php';

if (isset($_GET['id']) && (!is_numeric($_GET['id']) || is_array($_GET['id']))){
	header('location: ./new.php');
}

if (isset($_SESSION['draft'])){
	// back from confirm.php
	$draft = $_SESSION['draft'];
	$title = $draft -> title;
	$body  = $draft -> body;
	$is_article_for_kosensai = $draft -> is_article_for_kosensai;
	$kosensai_countdown = $draft -> kosensai_countdown;
	unset($_SESSION['draft']);
}elseif(isset($_POST['submit'])){
	// send some data input form
	if (isset($_POST['title']) && $_POST['title']){
		$title = $_POST['title'];
	}else{
		$title = '';
	}
	if (isset($_POST['body']) && $_POST['body']){
		$body = $_POST['body'];
	}else{
		$body = '';
	}
}elseif(isset($_GET['id'])){
	// initial load when editing
	$draft = new Diary($_GET['id']);
	$title = $draft -> title;
	$body  = $draft -> body;
	$is_article_for_kosensai = $draft -> is_article_for_kosensai;
	$kosensai_countdown = $draft -> kosensai_countdown;
	
	if(!$draft -> id){
		header('location: ./new.php');
		exit();
	}
}else{
	// initial load when opened this
	$title = '';
	$body  = '';
	$is_article_for_kosensai = false;
	$kosensai_countdown = getCountdown();
}

if (isset($_GET['id'])){
	$id   = $_GET['id'];
	$mode = '編集';
}else{
	$id   = null;
	$mode = '投稿';
}

if(isset($_POST['submit'])){
	$_SESSION['draft'] = new Diary($id, $title, $body);
	if ($_POST['is_article_for_kosensai'] == '1') {
		$_SESSION['draft'] -> is_article_for_kosensai = true;
	}
	if (empty($_POST['kosensai_countdown'])
		|| $_POST['kosensai_countdown'] == 0
		|| $_POST['kosensai_countdown'] == -1
		|| $_POST['kosensai_countdown'] == -2
		|| $_POST['kosensai_countdown'] < -2
	) {
		$_SESSION['draft'] -> kosensai_countdown = getCountdown();
	}else{
		$_SESSION['draft'] -> kosensai_countdown = $_POST['kosensai_countdown'];
	}
	
	if (!(is_admin() || is_modelator())
		&& $_SESSION['draft'] -> is_needPassword()
		&& isset($_POST['password'])
		&& !$_SESSION['draft'] -> verify($_POST['password'])
	){
		header("location: ${_SERVER['REQUEST_URI']}");
		exit();
	}
	if ((is_admin() || is_modelator())
		&& isset($_POST['is_change_password'])
		&& $_POST['is_change_password'] == '1'
		&& isset($_POST['set_password'])
		&& !empty($_POST['set_password'])
	){
		$_SESSION['draft'] -> setPassword($_POST['set_password']);
		$_SESSION['set_password'] = true;
	}
	header('location: ./confirm.php');
	exit();
}

$page = new Page("記事${mode}");
$page -> putHeader();
?>
<a href="./">トップ画面に戻る</a>

<section>
<h2>記事<?=$mode?></h2>
<form method="post">
<fieldset>

<label>記事を<?=$mode?>します。
  <input type="text" name="title" placeholder="タイトル" value="<?=h($title)?>" autofocus/>
  <br />
  <textarea id="diary-body" name="body" placeholder="本文"><?=h($body)?></textarea>
</label>
<?php
if(is_admin() || is_modelator()){
?>

<label>編集パスワード設定変更
  <input type="checkbox" name="is_change_password" value="1" />
  <input type="text" name="set_password" value="" placeholder="パスワード" required/>
</label>
<label>
  高専祭HP反映
  <input type="checkbox" name="is_article_for_kosensai" value="1" <?=($is_article_for_kosensai) ? 'checked' : ''?>/>
  <input type="text" name="kosensai_countdown" value="<?=h($kosensai_countdown)?>" placeholder="カウントダウン日数指定" required/>
  <small>入力ボックスは高専祭カウントダウンを記入。設定方法 =&gt; 3日前: 3, 1日目: 1d, 2日目: 2d, カウントダウン非表示: xx</small>
</label>
<?php
}else{
	if (isset($draft) && $draft -> is_needPassword()){
?>

<input type="password" name="password" value="" placeholder="編集パスワード" required/>
<?php
	}
}
?>

<div>
<input type="submit" name="submit" value="<?=$mode?>"/>
<button type="button" id="uploaded-imglist-link">画像リロード</button>
<a href="./image.php" target="_blank">画像アップロード</a>
</div>
<div id="uploaded-imglist" class="thumbnail-parent">
</div>
</fieldset>
</form>

<article>
<h3>本文プレビュー</h3>
<p id="diary-preview"></p>
</h3>
</article>
</section>
<script>
var button = $('#uploaded-imglist-link')
var body   = $('#diary-body')
button.click(loadImg)
button_default_text = button.html()

function loadImg(){
	$.ajax({
		type: 'GET',
		url:  'imglist.php',
		beforeSend: function(){
			button.html("読み込み中...")
		},
		success: function(msg){
			$('#uploaded-imglist').html("");
			$.each($.parseJSON(msg), function(){
				$('#uploaded-imglist').append($('<img>').attr('src', this.path).attr('style', 'cursor: pointer;').click(function(){
					var file_name = $(this).attr('src').split('/').pop()
					var tag = "[img " + file_name + "]"
					insertText("\n" + tag + "\n")
					syncPreview()
				}));
			})
			button.html(button_default_text)
		},
		error: function(){
			button.html("読み込みに失敗")
		}
	})
}
function syncPreview(){
	$.ajax({
		cache: false,
		type: 'POST',
		url:  'preview.php',
		processData: false,
		data: "body=" + encodeURIComponent(body.val()),
		success: function(msg){
			$('#diary-preview').html(msg)
		},
		error: function(){
			$('#diary-preview').html('<p>プレビューの読み込みに失敗</p>')
		}
	})
}

function insertText(msg){
	var spos = body.prop('selectionStart')
	var epos = body.prop('selectionEnd')
	var val = body.val()
	var vbefore = val.substring(0, spos)
	var vafter = val.substring(epos, val.length)
	body.val(vbefore + msg + vafter)
}

window.onload = function(){
	$('#diary-body').bind('input propartychange paste DOMAttrModified DOMSubtreeModified', syncPreview)
	syncPreview();
	
	$("input:checkbox").each(function(){
		$(this).next().prop("disabled", !$(this).prop("checked"));
	});
	$("input:checkbox").on("change", function () {
	    $(this).next().prop("disabled", !$(this).prop("checked"));
	});
}

$(document).keydown(function(e){
	if (e.which === 90 && e.ctrlKey){
		// Undo
	}
	if (e.which === 89 && e.ctrlKey){
		// Redo
	}
})

</script>
<?php
$page -> putFooter();
