CREATE TABLE diary(id integer primary key autoincrement,title text, body text, created_at text, updated_at text, status integer default 0, password text);
CREATE TABLE login_history(id integer primary key autoincrement, user_id integer, timestamp integer, ip text, success boolean);
CREATE TABLE user (id integer primary key autoincrement, name text unique, password text, email text, created_at integer, updated_at integer, is_admin boolean default 0, is_modelator boolean default 0, is_suspended boolean default 0);
