<?php
require_once 'utils.php';

define('DIARY_CREATED',       0);
define('DIARY_REJECTED',      4);
define('DIARY_ACCEPTED',      5);
define('DIARY_NON_GENERATED', 6);
define('DIARY_GENERATED',     7);
define('DIARY_PUBLISHING',    8);
define('DIARY_PUBLISHED',     9);

$diaryStatus = array();
$diaryStatus[DIARY_CREATED]       = '新規投稿';
$diaryStatus[DIARY_REJECTED]      = '要修正';
$diaryStatus[DIARY_ACCEPTED]      = '承認済';
$diaryStatus[DIARY_NON_GENERATED] = '生成待ち';
$diaryStatus[DIARY_GENERATED]     = '生成済';
$diaryStatus[DIARY_PUBLISHING]    = '公開処理中';
$diaryStatus[DIARY_PUBLISHED]     = '公開済';

class Diary{
	public static $db;
	public static $db_tbl_name;
	public $id;
	public $title;
	public $body;
	public $created_at, $updated_at;
	public $checked, $approved, $generated, $publishing, $published;
	public $updated;
	public $password, $password_raw;
	public $is_article_for_kosensai, $kosensai_countdown;
	
	static function setDB($db, $db_tbl_name){
		self::$db = $db;
		self::$db_tbl_name = $db_tbl_name;
	}
	function __construct($id = null, $title = '', $body = ''){
		$this -> id = $id;
		
		if ( !($id && $this -> db_fetch()) ){
			$this -> id         = null;
			$this -> title      = $title;
			$this -> body       = $body;
			$this -> created_at = time();
			$this -> updated_at = time();
			$this -> checked    = false;
			$this -> approved   = false;
			$this -> generated  = false;
			$this -> published  = false;
			$this -> publishing = false;
			$this -> updated    = false;
			$this -> password   = null;
			$this -> is_article_for_kosensai = false;
			$this -> kosensai_countdown = '';
		}
		
		
		if(!empty($title)){
			$this -> title = $title;
		}
		if(!empty($body)){
			$this -> body = $body;
		}
	}
	function db_fetch(){
		$res = self::db_load($this -> id);
		
		if (!$res){
			return false;
		}
		
		foreach($res as $k => $v){
			$this -> $k = $v;
		}
		
		$this -> setStatus($res['status'], false);

		if ($res['site']) {
			$this -> is_article_for_kosensai = true;
		}else {
			$this -> is_article_for_kosensai = false;
		}
		if ($res['countdown']) {
			$this -> kosensai_countdown = $res['countdown'];
		}else {
			$this -> kosensai_countdown = '';
		}
		
		return true;
	}
	function prevId(){
		$all = self::$db -> fetchWithExpr(self::$db_tbl_name, 'status >= '.DIARY_ACCEPTED, true);
		foreach($all as $k => $v){
			if ($all[$k]['id'] == $this -> id){
				if($k + 1 < count($all)){
					return $all[$k + 1]['id'];
				}
				break;
			}
		}
		return null;
	}
	function nextId(){
		$all = self::$db -> fetchWithExpr(self::$db_tbl_name, 'status >= '.DIARY_ACCEPTED, true);
		foreach($all as $k => $v){
			if ($all[$k]['id'] == $this -> id){
				if ($k - 1 >= 0){
					return $all[$k - 1]['id'];
				}
				break;
			}
		}
		return null;
	}
	function toArray(){
		return array(
			'id'         => $this -> id,
			'title'      => $this -> title,
			'body'       => $this -> body,
			'created_at' => $this -> created_at,
			'updated_at' => $this -> updated_at,
			'approved'   => $this -> approved,
			'generated'  => $this -> generated,
			'updated'    => $this -> updated,
			'site'       => (int)$this -> is_article_for_kosensai,
			'countdown'  => $this -> kosensai_countdown,
		);
	}
	function save($save_updated_at = true){
		$updated_at = ($save_updated_at) ? time() : $this -> updated_at;
		if ($this -> id){
			$res = self::$db -> update(self::$db_tbl_name, array(
				'id'         => $this -> id,
				'title'      => $this -> title,
				'body'       => $this -> body,
				'created_at' => $this -> created_at,
				'updated_at' => $updated_at,
				'status'     => $this -> getStatus(),
				'password'   => $this -> password,
				'site'       => $this -> is_article_for_kosensai,
				'countdown'  => $this -> kosensai_countdown,
			),
			'id = '. $this -> id);
			$this -> updated = true;
		}else{
			$res = self::$db -> insert(self::$db_tbl_name, array(
				'title'      => $this -> title,
				'body'       => $this -> body,
				'created_at' => $this -> created_at,
				'updated_at' => $this -> updated_at,
				'status'     => $this -> getStatus(),
				'password'   => $this -> password,
				'site'       => $this -> is_article_for_kosensai,
				'countdown'  => $this -> kosensai_countdown,
			));
			$this -> id = self::$db -> getInstance() -> lastInsertId();
		}
		
		return $res;
	}
	function delete(){
		return self::$db -> delete('diary', 'id = ' . $this -> id);
	}
	function setStatus($const_status, $sync_db = true){
		$newStatus = $const_status;
		$beforeStatus = $this -> getStatus();
		
		switch ($const_status){
		case DIARY_CREATED:
			$this -> checked    = false;
			$this -> approved   = !USE_APPROVAL;
			$this -> generated  = false;
			$this -> publishing = false;
			$this -> published  = false;
			break;
		case DIARY_REJECTED:
			$this -> checked    = true;
			$this -> approved   = false;
			$this -> generated  = false;
			$this -> publishing = false;
			$this -> published  = false;
			break;
		case DIARY_ACCEPTED:
			$this -> checked    = true;
			$this -> approved   = true;
			$this -> generated  = false;
			$this -> publishing = false;
			$this -> published  = false;
			break;
		case DIARY_NON_GENERATED:
			$this -> checked    = USE_APPROVAL;
			$this -> approved   = true;
			$this -> generated  = false;
			$this -> publishing = false;
			$this -> published  = false;
			break;
		case DIARY_GENERATED:
			$this -> checked    = USE_APPROVAL;
			$this -> approved   = true;
			$this -> generated  = true;
			$this -> publishing = false;
			$this -> published  = false;
			break;
		case DIARY_PUBLISHING:
			$this -> checked    = USE_APPROVAL;
			$this -> approved   = true;
			$this -> generated  = true;
			$this -> publishing = true;
			$this -> published  = false;
			break;
		case DIARY_PUBLISHED:
			$this -> checked    = USE_APPROVAL;
			$this -> approved   = true;
			$this -> generated  = true;
			$this -> publishing = false;
			$this -> published  = true;
			break;
		default:
			return false;
		}
		
		if ($sync_db){
			if ($beforeStatus >= $newStatus){
				$prev = new Diary($this -> prevId());
				$next = new Diary($this -> nextId());
				
				$this -> save();
			}else{
				$this -> save();
				
				$prev = new Diary($this -> prevId());
				$next = new Diary($this -> nextId());
			}
			
			if ($prev && $prev -> getStatus() >= DIARY_ACCEPTED){
				$prev -> setStatus(DIARY_NON_GENERATED, false);
				$prev -> save(false);
			}
			
			if ($next && $next -> getStatus() >= DIARY_ACCEPTED){
				$next -> setStatus(DIARY_NON_GENERATED, false);
				$next -> save(false);
			}
		}
		
		return true;
	}
	function getStatus(){
		$checked    = $this -> checked;
		$approved   = $this -> approved;
		$generated  = $this -> generated;
		$publishing = $this -> publishing;
		$published  = $this -> published;
		
		if (USE_APPROVAL){
			if ($checked){
				if($approved){
					if($generated){
						if($published){
							return DIARY_PUBLISHED;
						}else{
							if($publishing){
								return DIARY_PUBLISHING;
							}else{
								return DIARY_GENERATED;
							}
						}
					}else{
						return DIARY_ACCEPTED;
					}
				}else{
					return DIARY_REJECTED;
				}
			}else{
				return DIARY_CREATED;
			}
		}else{
			if($generated){
				if($published){
					return DIARY_PUBLISHED;
				}else{
					if($publishing){
						return DIARY_PUBLISHING;
					}else{
						return DIARY_GENERATED;
					}
				}
			}else{
				return DIARY_NON_GENERATED;
			}
		}
	}
	function is_needPassword(){
		return !empty($this -> password);
	}
	function setPassword($password_raw){
		if (empty($password_raw)){
			$password = '';
		}else{
			$this -> password_raw = $password_raw;
			$this -> password = password_hash($password_raw, PASSWORD_DEFAULT);
		}
	}
	function verify($password){
		if ($res = password_verify($password, $this -> password)){
			
			if (password_needs_rehash($this -> password, PASSWORD_DEFAULT)){
				$this -> password = password_hash($password, PASSWORD_DEFAULT);
				$this -> save();
			}
		}
		return $res || empty($this -> password);
	}
	
	function getFilename(){
		return date("Y-m-d-H-i-s", $this -> created_at);
	}
	
	static function db_load($id){
		if (!$id){
			return null;
		}
		if (count($res = self::$db -> fetchWithExpr(self::$db_tbl_name, 'id = ' . $id, 1)) <= 0){
			return null;
		}
		if (empty($res[0])){
			return null;
		}
		
		return $res[0];
	}
}
