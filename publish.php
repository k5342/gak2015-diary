<?php
require 'include.php';

function getFilename($row){
	return date("Y-m-d-H-i-s", $row['created_at']);
}

try{
	$db = new SQLite('diary.sqlite');
	
	$res = $db -> fetchWithExpr('diary', 'status = ' . DIARY_GENERATED, true);

	if(count($res) <= 0){
		echo "既に全ての記事の処理が完了しています\n";
		exit();
	}
	
	if(!$c = ftp_connect('tctgak.web.fc2.com')){
		throw new Exception('FTP接続時のエラーです');
	}
	if (!ftp_login($c, 'tctgak', getenv('GAK_FC2_FTP_PASS'))){
		throw new Exception('FTPでログインできませんでした');
	}
	
	@ftp_mkdir($c, '/diary');
	@ftp_mkdir($c, '/diary/images');
	
	foreach($res as $r){
		$fname = "". getFilename($r);
		
		$db -> update('diary', array('status' => DIARY_PUBLISHING), 'id = '.$r['id']);
		
		if ($r['site']){
			$fpath = '/kosensai/diary/';
		}else{
			$fpath = '/diary/';
		}
		
		if(ftp_put($c, "$fpath$fname.html", "./outputs/$fname.html", FTP_ASCII) !== FALSE){
			$db -> update('diary', array('status' => DIARY_PUBLISHED), 'id = '.$r['id']);
		}else{
			$db -> update('diary', array('status' => DIARY_GENERATED), 'id = '.$r['id']);
			throw new Exception('ファイル転送中のエラーです');
		}
	}
	
	if(ftp_put($c, "/news", "./outputs/news", FTP_ASCII) === FALSE){
		throw new Exception('ファイル転送中のエラーです');
	}
	if(ftp_put($c, "/kosensai/kosensai_news", "./outputs/kosensai_news", FTP_ASCII) === FALSE){
		throw new Exception('ファイル転送中のエラーです');
	}
	
	echo "記事転送正常終了\n";
}catch(Exception $e){
	echo $e -> getMessage() . "\n";
}

$path_list = glob("./images/*.{gif,jpg,png}", GLOB_NOSORT | GLOB_BRACE);

foreach($path_list as $path){
	$fname = basename($path);
	if(ftp_put($c, "/diary/images/$fname", $path, FTP_ASCII) === FALSE){
		echo "画像転送中のエラーです：$path\n";
	}
}

echo "画像転送処理を終了します\n";
