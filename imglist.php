<?php
require 'include.php';

$path_list = glob("./images/*.{gif,jpg,png}", GLOB_NOSORT | GLOB_BRACE);
$file_list = array();
foreach($path_list as $path)
$file_list []= array(
	'path' => $path
);
echo json_encode($file_list);
