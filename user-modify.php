<?php
require 'include.php';

if(!(is_admin() || is_modelator())){
	header('location: ./user-manage.php');
	exit();
}

if (!isset($_GET['name']) || is_array($_GET['name'])){
 	header('location: ./user-manage.php');
	exit();
}

$u = new User(null, $_GET['name']);
if ($u -> id === null
	|| (!is_admin() && $u -> is_admin)
){
	header('location: ./user-manage.php');
	exit();
}

if(isset($_POST['submit'])
	&& (
		(is_admin() || is_modelator())
		&& (!is_admin() && !$u -> is_admin)
		|| is_admin()
	)
){
	if(!isset($_POST['token']) || !verify_token($_POST['token'])){
		header('location: ./user-modify.php');
		exit();
	}
	
	if (isset($_POST['password']) && !empty($_POST['password'])){
		$u -> password = password_hash($_POST['password'], PASSWORD_DEFAULT);
	}
	if (isset($_POST['email'])){
		$u -> email = $_POST['email'];
	}
	$u -> is_admin     = (bool)(($u -> id === $_SESSION['user'] -> id || isset($_POST['is_admin'])) && is_admin());
	$u -> is_modelator = (bool)(($u -> id === $_SESSION['user'] -> id) && is_modelator() || isset($_POST['is_modelator']) && (is_admin() || is_modelator()));
	$u -> save();
	
	header('location: ./user-modify.php');
	exit();
}

regenerate_token();
$token = getToken();

$page = new Page("ユーザ編集");
$page -> putHeader();
?>

<a href="./user-manage.php">ユーザ管理画面に戻る</a>

<section>
<h2>ユーザ編集</h2>
<form method="post">
<fieldset>
<label>
	ログイン名:
	<span><?=h($u -> name)?></span>
</label>
<br />
<label>
	新しいパスワード:
	<input type="text" name="password" value="" autocomplete="off" />
</label>
<br />
<label>
	メール:
	<input type="email" name="email" value="<?=h($u -> email)?>" />
</label>
<br />
<label>
	管理権限
	<input type="checkbox" name="is_admin" value="1" <?=($u -> is_admin) ? 'checked' : ''?> <?=(is_admin() && $u -> id !== $_SESSION['user'] -> id) ? '' : 'disabled'?>/>
<br />
<label>
	準管理権限
	<input type="checkbox" name="is_modelator" value="1" <?=($u -> is_modelator) ? 'checked' : ''?> <?=((is_admin() || is_modelator()) && $u -> id !== $_SESSION['user'] -> id || is_admin()) ? '' : 'disabled'?>/>
</label>
<input type="hidden" name="token" value="<?=$token?>" />
<br />
<input type="submit" name="submit" value="編集" />
<p>
登録日時: <?=date('Y-m-d H:i:s', $u -> created_at)?>
<br />
更新日時: <?=date('Y-m-d H:i:s', $u -> updated_at)?>
</p>
</fieldset>
</form>
</section>
<?php
$page -> putFooter();
