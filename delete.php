<?php
require 'include.php';

if(!isset($_GET['id'])
	|| !isset($_GET['token'])
	|| $_GET['token'] !== $_SESSION['token']
){
	header('location: ./');
	exit();
}

if(isset($_GET['id'])){
	$id = $_GET['id'];
	
	$article = new Diary($id);
	
	$res = true;
	if ($article -> getStatus() >= DIARY_ACCEPTED){
		$res &= @unlink("./outputs/" . $article -> getFilename() . ".html");
	}
	
	$article -> setStatus(DIARY_ACCEPTED);
	
	$res &= $article -> delete();
	
	if ($res){
		$msg = '<p class="sysmsg info">記事「' . h($article -> title) . '」を削除しました。</p>';
	}else{
		$msg = '<p class="sysmsg error">書き込みエラー。</p>';
	}
}
$page = new Page("記事削除");
$page -> putHeader();
?>
<?=@$msg?>

<a href="./">トップ画面に戻る</a>

<?php
$page -> putFooter();
