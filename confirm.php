<?php
require 'include.php';

$done = false;
	
if(isset($_SESSION['draft'])){
	$article = $_SESSION['draft'];
	$id       = $article -> id;
	$title    = $article -> title;
	$body     = $article -> body;
	$password_raw = $article -> password_raw;
	$is_article_for_kosensai = $article -> is_article_for_kosensai;
	$kosensai_countdown = $article -> kosensai_countdown;
}else{
	header('location: ./new.php');
	exit();
}

$mode = ($id) ? '編集' : '投稿';

if(isset($_POST['cancel'])){
	if($id){
		header("location: ./new.php?id=${id}");
		exit();
	}else{
		header('location: ./new.php');
		exit();
	}
}

if(isset($_POST['confirm'])){
	$article -> title = $title;
	$article -> body  = $body;
	$article -> setStatus(DIARY_CREATED);
	$res = $article -> save();
	
	if ($res){
		$msg = '<p class="sysmsg info">' . $mode . '完了しました。</p>';
		unset($_SESSION['draft']);
		unset($_SESSION['password_raw']);
		
		$done = true;
	}else{
		$msg = '<p class="sysmsg error">書き込みエラー。</p>';
		$done = false;
	}
}
$page = new Page("${mode}確認");
$page -> putHeader();
?>
<?=@$msg?>

<?php
if($done){
?>
<a href="./">トップ画面に戻る</a>
<?php
}else{
?>
	<p>以下の内容で記事を<?=$mode?>します。</p>
<article>
	<h2><?=h($title)?></h2>
	<p>
		<?=n2br(expandTag(h($body)))?>
	</p>
</article>

<?php
if(is_admin() || is_modelator()){
?>

<p>
<strong>編集パスワード: </strong>
<?php
	if (empty($password_raw)){
		if (isset($_SESSION['set_password'])){
			echo '(無し)';
		}else{
			echo '(変更無し)';
		}
	}else{
		echo h($password_raw);
	}
?>
</p>
<p>
<strong>高専祭のページへの反映: <?=($article -> is_article_for_kosensai) ? '有(カウントダウン:' . h($article -> kosensai_countdown) . ')</strong>' : '</strong>無'?></p>
<?php
}
?>

<form method="post">
<input type="submit" name="confirm" value="<?=$mode?>" />
<input type="submit" name="cancel" value="<?=$mode?>画面に戻る" />
</form>
<?php
}
$page -> putFooter();
