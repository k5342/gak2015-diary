<?php
require_once 'utils.php';

class Page{
	public $name = "";
	public $description = "";
	public $loginRequired = true;
	
	function __construct($name = "", $description = ""){
		$this -> name = $name;
		$this -> description = $description;
		
		# generate <title>
		if(empty($name)){
			$this -> title = '学生会HP記事管理システム';
		}else{
			$this -> title = $name .' | 学生会HP記事管理システム';
		}
	}
	function loginRequired($mode = true){
		$this -> loginRequired = (bool)$mode;
	}
	function putHeader(){
		if($this -> loginRequired && !is_login()){
			header("location: ./login.php?continue=${_SERVER['REQUEST_URI']}");
			exit();
		}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
 
  <title><?=h($this -> title)?></title>
  <meta name="description" content="<?=h($this -> description)?>">
 
  <meta name="viewport" content="width=1000px">
 
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
 
  <link rel="stylesheet" href="./styles.css">
  <link rel="stylesheet" href="./normalize.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="./jquery.min.js"></script>
</head>
<body>
<div id="container">
<?php
	}
	function putFooter(){
?>
</div>
</body>
</html>
<?php
	}
}
