<?php
require_once 'template.php';
require_once 'diary.php';
require_once 'user.php';
require_once 'utils.php';
require_once 'config.php';


Diary::setDB(new SQLite('diary.sqlite'), 'diary');
User::setDB(new SQLite('diary.sqlite'), 'user');

$c = session_get_cookie_params();
session_set_cookie_params($c['lifetime'], dirname($_SERVER['SCRIPT_NAME']));

session_start();

if (is_login()){
	$_SESSION['user'] = new User(null, $_SESSION['user'] -> name);
}
