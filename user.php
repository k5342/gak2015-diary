<?php
require_once 'utils.php';

class User{
	public static $db;
	public static $db_tbl_name;
	public $id;
	public $name;
	public $password, $email;
	public $created_at, $updated_at;
	public $is_admin, $is_modelator, $is_suspended;
	
	static function setDB($db, $db_tbl_name){
		self::$db = $db;
		self::$db_tbl_name = $db_tbl_name;
	}
	function __construct($id = null, $name = '', $password = '', $email = ''){
		$this -> id = $id;
		
		if ( !($name && $this -> db_fetch($name)) ){
			$this -> id           = null;
			$this -> name         = $name;
			$this -> password     = $password;
			$this -> email        = $email;
			$this -> created_at   = time();
			$this -> updated_at   = time();
			$this -> is_admin     = false;
			$this -> is_modelator = false;
			$this -> is_suspended = false;
		}
		
		
		#if(!empty($name)){
		#	$this -> name = $name;
		#}
		#if(!empty($body)){
		#	$this -> body = $body;
		#}
	}
	function db_fetch($key = null){
		$key = ($key) ? $key : $this -> name;
		$res = self::db_load($key);
		
		if (!$res){
			return false;
		}
		
		foreach($res as $k => $v){
			$this -> $k = $v;
		}
		
		return true;
	}

	function toArray(){
		return array(
			'id'           => $this -> id,
			'name'         => $this -> name,
			'password'     => $this -> password,
			'email'        => $this -> email,
			'created_at'   => $this -> created_at,
			'updated_at'   => $this -> updated_at,
			'is_admin'     => $this -> is_admin,
			'is_modelator' => $this -> is_modelator,
			'is_suspended' => $this -> is_suspended,
		);
	}
	function save($save_updated_at = true){
		$data = $this -> toArray();
		$data['updated_at'] = time();
		
		if ($this -> id){
			$res = self::$db -> update(self::$db_tbl_name, $data, 'id = '. $this -> id);
		}else{
			$data['created_at'] = $data['updated_at'];
			$res = self::$db -> insert(self::$db_tbl_name, $data);
			$this -> id = self::$db -> getInstance() -> lastInsertId();
		}
		
		return $res;
	}
	function delete(){
		return self::$db -> delete('diary', 'id = ' . $this -> id);
	}
	static function db_load($name){
		
		if (!$name){
			return null;
		}
		if (count($res = self::$db -> fetchWithExpr(self::$db_tbl_name, 'name = ' . self::$db -> getInstance() -> quote($name), 1)) <= 0){
			return null;
		}
		if (empty($res[0])){
			return null;
		}
		
		return $res[0];
	}
}
