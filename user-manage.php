<?php
require 'include.php';

if(!(is_admin() || is_modelator())){
	header('location: ./');
	exit();
}

$db = new SQLite('diary.sqlite');

if(isset($_GET['a'])
	&& (is_admin() || is_modelator())
){
	if(!isset($_GET['token']) || !verify_token($_GET['token'])){
		header('location: ./user-manage.php');
		exit();
	}
	
	if($_GET['a'] === 'delete' && isset($_GET['name'])){
		if($_SESSION['user'] -> name !== $_GET['name']){
			$db -> delete('user', 'name = ' . $db -> getInstance() -> quote($_GET['name']));
		}
	}
	
	if($_GET['a'] === 'register'){
		if(isset($_POST['name'])
			&& isset($_POST['password'])
			&& isset($_POST['email'])
			&& !empty($_POST['name'])
			&& !empty($_POST['password'])
			&& preg_match('/[\w_]+/', $_POST['name'])
		){
			$u = new User(null,
				$_POST['name'],
				password_hash($_POST['password'], PASSWORD_DEFAULT),
				$_POST['email']
			);
			if($u -> id === null){
				$u -> is_admin     = (bool)(isset($_POST['is_admin']) & is_admin());
				$u -> is_modelator = (bool)(isset($_POST['is_modelator']) & (is_admin() | is_modelator()));
				$u -> save();
			}
		}
	}
	
#	header('location: ./user-manage.php');
#	exit();
}

$res = $db -> fetchAll('user', true);

regenerate_token();
$token = getToken();

$page = new Page("ユーザ管理");
$page -> putHeader();
?>

<a href="./">トップ画面に戻る</a>

<section>
<h2>ユーザ管理</h2>
<table class="lined">
	<tbody>
	<tr>
		<th class="nowrap">#</th>
		<th>login</th>
		<th>パスワード</th>
		<th>email</th>
		<th>管理者</th>
		<th>準管理者</th>
		<th></th>
	</tr>
	<tr>
			<form method="post" action="./user-manage.php?a=register&token=<?=$token?>">
			<td></td>
			<td><input type="text" name="name" value="" pattern="[\w_-]+" placeholder="新規登録" required/></td>
			<td><input type="text" name="password" value="" required/></td>
			<td><input type="email" name="email" value="" /></td>
			<td><input type="checkbox" name="is_admin" value="1" <?=(is_admin()) ? '' : 'disabled'?>/></td>
			<td><input type="checkbox" name="is_modelator" value="1" <?=(is_admin() | is_modelator()) ? '' : 'disabled'?>/></td>
			<td><input type="submit" name="submit" value="追加"></td>
		</form>
	</tr>
<?php
foreach($res as $u){
?>
	<tr>
		<td><?=$u['id']?></td>
		<td><a href="./user-modify.php?name=<?=$u['name']?>"><?=$u['name']?></a></td>
		<td>********</td>
		<td><?=$u['email']?></td>
		<td><?=($u['is_admin']) ? '有' : ''?></td>
		<td><?=($u['is_modelator']) ? '有' : ''?></td>
		<td>
			<p><a href="./user-manage.php?a=delete&token=<?=$token?>&name=<?=$u['name']?>">削除</p>
		</td>
	</tr>
<?php
}
?>
	</tr>
	</tbody>
</table>
</section>
<?php
$page -> putFooter();
