<?php
require 'include.php';

if (isset($_GET['id']) && (!is_numeric($_GET['id']) || is_array($_GET['id']))){
	header('location: ./');
}

$article = new Diary($_GET['id']);
$title = $article -> title;
$body  = $article -> body;

if(!$article -> id){
	header('location: ./');
	exit();
}

$id           = $article -> id;
$title        = $article -> title;
$body         = $article -> body;
$status       = $article -> getStatus();
$created_at   = $article -> created_at;
$updated_at   = $article -> updated_at;
$display_date = "Posted at " . date("Y/m/d H:i:s", $created_at);

if($created_at != $updated_at){
	$display_date .= ", Updated at " . date("Y/m/d H:i:s", $updated_at);
}

if ($article -> is_article_for_kosensai){
	$caption = '<small>※ この記事は高専祭HPに同期されます</small>';
}

$page = new Page($article -> title);
$page -> putHeader();
$page -> loginRequired(true);
?>
<a href="./">トップ画面に戻る</a>

<section>
	<article>
		<h3>
			<?=h($title)?>
			<div class="postinfo">[<?=h($diaryStatus[$status])?>] <?=h($display_date)?></div>
		</h3>
		<div>
			<a href="./new.php?id=<?=$id?>">編集</a>
			<p>
				<?=@$caption?>

			</p>
		</div>
		<section><?=n2br(expandTag(h($body)))?></section>
	</article>
</section>

<?php
$page -> putFooter();
