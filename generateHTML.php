<?php
require 'include.php';

# =================
# I M P O R T A N T
# =================

ini_set('display_errors', 'Off');

# =================

$db = new SQLite('diary.sqlite');

if (USE_APPROVAL){
	# 教員チェックありの実装
	$res = $db -> fetchWithExpr('diary', 'status >= ' . DIARY_ACCEPTED, true);
	$fes = $db -> fetchWithExpr('diary', 'status >= ' . DIARY_ACCEPTED . ' AND site = 1', true);
}else{
	# 教員チェックなしの実装
	$res = $db -> fetchWithExpr('diary', 'status >= ' . DIARY_CREATED, true);
	$fes = $db -> fetchWithExpr('diary', 'status >= ' . DIARY_CREATED . ' AND site = 1', true);
}

$res2 = $db -> fetchWithExpr('diary', 'status = ' . DIARY_PUBLISHED, true);

if (count($res) == count($res2)){
	echo "全ての記事が公開済なので終了します\n";
	exit;
}

function getPath($row, $row_current) {
	if ($row['site']) {
		if ($row_current['site']) {
			return './';
		}else{
			return '../kosensai/diary/';
		}
	}else{
		if ($row_current['site']) {
			return '../../diary/';
		}else{
			return './';
		}
	}
}
function getFilename($row){
	return date("Y-m-d-H-i-s", $row['created_at']);
}

$top_page_list = '';
$kosensai_top_page_list = '';

$fes_article_list = '<ul>';
foreach($fes as $f){
	$fes_article_list .= "<li>";
	$fes_article_list .= date("Y/m/d H:i", $f['updated_at']);
	$fes_article_list .= "<br />";
	$fes_article_list .= '<a href="./'. getFilename($f) .'.html">'. $f['title'] .'</a>';
	$fes_article_list .= "</li>";

	if (mb_strlen($f['body']) < 100){
		$short_body = '';
	}else{
		$short_body = '' . n2br(mb_substr(stripTag(chomp($f['body'])), 0, 100)) . '... ';
	}
	
	$kosensai_top_page_list .= '<article>';
	$kosensai_top_page_list .= '<h3>';
	$kosensai_top_page_list .= '<div class="postinfo">' . date("Y年m月d日 H時i分 更新", $f['updated_at']) . "</div>";
	$kosensai_top_page_list .= h($f['title']);
	$kosensai_top_page_list .= '</h3>';
	$kosensai_top_page_list .= "<p>";
	$kosensai_top_page_list .= $short_body;
	$kosensai_top_page_list .= '<a href="./diary/' . getFilename($f) . '.html" target="_blank">続きを読む</a>';
	$kosensai_top_page_list .= "</p>";
	$kosensai_top_page_list .= '</article>';
}
$fes_article_list .= '</ul>';

foreach($res as $key => $r){
	$prev = ($key + 1 >= count($res)) ? null : $key + 1;
	$next = ($key - 1 < 0)            ? null : $key - 1;
	
	$title = $r['title'];
	$body = $r['body'];
	$created_at = $r['created_at'];
	$updated_at = $r['updated_at'];
	$display_date = "Posted at " . date("Y/m/d", $created_at);
	
	if($created_at != $updated_at){
		$display_date .= ", Updated at " . date("Y/m/d", $updated_at);
	}
	
	if ($r['site']){
		$froot = './kosensai/diary/';
	}else{
		$froot = './diary/';
	}
	
	$fname = "". getFilename($r);

	$top_page_list .= '<li>'.PHP_EOL;
	$top_page_list .= '  <ul>'.PHP_EOL;
	$top_page_list .= '    <li><strong>' . date("Y年n月j日", $created_at) . '</strong></li>'.PHP_EOL;
	$top_page_list .= '    <li><a href="'. $froot . $fname . '.html">' . h($title) . '</a></li>'.PHP_EOL;
	$top_page_list .= '  </ul>'.PHP_EOL;
	$top_page_list .= '</li>'.PHP_EOL;
	
	ob_start();
	
	if ($r['site']){
		$countdown_panel = ($r['countdown']) ? $r['countdown'] : 'xx';
		
		$countdown_prefix = '第41回高専祭';
		$countdown_suffix = '';
		if ($countdown_panel != '1d'
			&& $countdown_panel != '2d'
			&& $countdown_panel != 'xx'
		){
			$countdown_prefix .= 'まで';
			$countdown_suffix .= '日';
		}
		$countdown_panel_array = str_split($countdown_panel);
		include 'template_kosensai.php';
	}else{
		include 'template_gak2015.php';
	}
	
	$html = ob_get_contents();
	ob_end_clean();
	
	$fpath = "./outputs/$fname.html";
	if(file_put_contents($fpath, $html) !== FALSE){
		chmod($fpath, 0664);
		$db -> update('diary', array('status' => DIARY_GENERATED), 'id = ' . $r['id']);
	}
}

file_put_contents("./outputs/news", $top_page_list);
chmod("./outputs/news", 0664);
file_put_contents("./outputs/kosensai_news", $kosensai_top_page_list);
chmod("./outputs/kosensai_news", 0664);
